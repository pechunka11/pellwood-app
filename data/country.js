export default {
  cz: [
    {name: 'cz', value: 'Česko'},
    {name: 'sk', value: 'Slovensko'},
  ],
  en: [
    {name: 'de', value: 'Deutschland'},
    {name: 'at', value: 'Austria'}
  ]
}
