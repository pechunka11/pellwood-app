export default {
  products: {
    cz: "Produkty",
    en: "Products",
    de: "xxx"
  },
  search: {
    cz: "Hledat",
    en: "Search",
    de: "xxx"
  },
  noresult: {
    cz: "Nic jsme nenašli, zkuste jiné slovo.",
    en: "We didn't find anything, try another word.",
    de: "xxx"
  },
  user: {
    cz: "Uživatel",
    en: "User",
    de: "xxx"
  },
  login: {
    cz: "Přihlásit se",
    en: "Login",
    de: "xxx"
  },
  login2: {
    cz: "Přihlásit",
    en: "Login",
    de: "xxx"
  },
  formemail: {
    cz: "e-mail",
    en: "email",
    de: "xxx"
  },
  formpassword: {
    cz: "heslo",
    en: "password",
    de: "xxx"
  },
  forgottenpassword: {
    cz: "Zapomenuté heslo",
    en: "Reset your password",
    de: "xxx"
  },
  notyetaccount: {
    cz: "Nemáte ještě účet?",
    en: "Create account",
    de: "xxx"
  },
  registration: {
    cz: "Registrace",
    en: "Registration",
    de: "xxx"
  },
  intobasket: {
    cz: "Do košíku",
    en: "Into the basket",
    de: "xxx"
  },
  checkout: {
    cz: "K objednávce",
    en: "Checkout",
    de: "xxx"
  },
  basket: {
    cz: "Košík",
    en: "Basket",
    de: "xxx"
  },
  emptybasket: {
    cz: "Váš košík je prázdný.",
    en: "Basket is empty",
    de: "xxx"
  },
  delivery: {
    cz: "Doprava",
    en: "Delivery",
    de: "xxx"
  },
  totalprice: {
    cz: "Celková cena",
    en: "Total price",
    de: "xxx"
  },
  ordersummary: {
    cz: "Souhrn objednávky",
    en: "Order summary",
    de: "xxx"
  },
  payment: {
    cz: "Platba",
    en: "Payment",
    de: "xxx"
  },
  contact: {
    cz: "Kontaktní údaje",
    en: "Contact information",
    de: "xxx"
  },
  formphone: {
    cz: "telefon",
    en: "phone",
    de: "xxx"
  },
  formname: {
    cz: "jméno",
    en: "name",
    de: "xxx"
  },
  formsurname: {
    cz: "příjmení",
    en: "surname",
    de: "xxx"
  },
  formstreet: {
    cz: "ulice a č.p.",
    en: "street and no.",
    de: "xxx"
  },
  formcity: {
    cz: "město",
    en: "city",
    de: "xxx"
  },
  formzip: {
    cz: "PSČ",
    en: "ZIP",
    de: "xxx"
  },
  formstate: {
    cz: "stát",
    en: "state",
    de: "xxx"
  },
  checkdifferentadress: {
    cz: "Doručit na jinou adresu",
    en: "Deliver to another address",
    de: "xxx"
  },
  checkcompanydata: {
    cz: "Doplnit firemní údaje",
    en: "Fill in the company data",
    de: "xxx"
  },
  chcekcreatecaccout: {
    cz: "Založit účet pro příští objednávky",
    en: "Create account",
    de: "xxx"
  },
  chceknote: {
    cz: "Poznámka k objednávce",
    en: "Add note to order",
    de: "xxx"
  },
  infovat: {
    cz: "Všechny ceny jsou včetně DPH 21 %",
    en: "All prices include 21% VAT",
    de: "xxx"
  },
  infoterm: {
    cz: "Odesláním objednávky souhlasíte s obchodními podmínkami.",
    en: "By sending the order, you agree with the terms and conditions.",
    de: "xxx"
  },
  sendorder: {
    cz: "Odeslat objednávku",
    en: "Send order",
    de: "xxx"
  },
  backtohp: {
    cz: "Zpět na hlavní stránku",
    en: "Back to home page",
    de: "xxx"
  },
  buy: {
    cz: "Koupit",
    en: "Buy",
    de: "xxx"
  },
  homepage: {
    cz: "Hlavní stránka",
    en: "Homepage",
    de: "xxx"
  },
  price: {
    cz: "Cena",
    en: "Price",
    de: "xxx"
  },
  unitczk: {
    cz: "Kč",
    en: "CZK",
    de: "xxx"
  },
  pc: {
    cz: "ks",
    en: "pc",
    de: "xxx"
  },
  selectvariant: {
    cz: "Vybrat variantu",
    en: "Select variant",
    de: "xxx"
  },
  interestedproducts: {
    cz: "Mohlo by vás zajímat",
    en: "You could be interested in",
    de: "xxx"
  },
  footeradress: {
    cz: "Adresa",
    en: "Adress",
    de: "xxx"
  },
  footercontact: {
    cz: "Kontakt",
    en: "Contact",
    de: "xxx"
  },
  footerabout: {
    cz: "Informace o nákupu",
    en: "About shopping",
    de: "xxx"
  },
  footerfollowus: {
    cz: "Sledujte nás",
    en: "Follow us",
    de: "xxx"
  },
  moreinformation: {
    cz: "Více informací",
    en: "More information",
    de: "xxx"
  },
  back: {
    cz: "Zpět",
    en: "Back",
    de: "xxx"
  },
  stock: {
    cz: "Skladem",
    en: "In stock",
    de: "xxx"
  },
  nosctock: {
    cz: "Není skladem",
    en: "xxx",
    de: "xxx"
  },
  onorder: {
    cz: "Na objednávku",
    en: "xxx",
    de: "xxx"
  },
  color: {
    cz: "Barva",
    en: "Color",
    de: "xxx"
  },
  from: {
    cz: "od",
    en: "from",
    de: "xxx"
  },
  allProducts: {
    cz: "Všechny produkty",
    en: "All products",
    de: "xxx"
  },
  searchAndFilter: {
    cz: "Hledat a filtrovat",
    en: "Search and filter",
    de: "xxx"
  },
  filter: {
    cz: "Filtrovat",
    en: "Filter",
    de: "xxx"
  },
  addToBasket: {
    cz: "PŘIDAT DO KOŠÍKU",
    en: "Add to basket",
    de: "xxx"
  },
  free: {
    cz: "ZDARMA",
    en: "FREE",
    de: "xxx"
  },
  yourBasket: {
    cz: "Váš nákupní košík",
    en: "Basket",
    de: "xxx"
  },
  item: {
    cz: "Položka",
    en: "Item",
    de: "xxx"
  },
  quantity: {
    cz: "Počet",
    en: "Quantity",
    de: "xxx"
  },
  remove: {
    cz: "Odstranit",
    en: "Remove",
    de: "xxx"
  },
  deliveryDetails: {
    cz: "Dodací údaje",
    en: "Delivery details",
    de: "xxx"
  },
  order: {
    cz: "Objednávka",
    en: "Order",
    de: "xxx"
  },
  editItems: {
    cz: "UPRAVIT POLOŽKY",
    en: "Edit items",
    de: "xxx"
  },
  notSelected: {
    cz: "Nevybráno",
    en: "Not selected",
    de: "xxx"
  },
  accessCondition1: {
    cz: "Odesláním objednávky souhlasíte s",
    en: "By sending the order, you agree with the",
    de: "xxx"
  },
  accessCondition2: {
    cz: "obchodními podmínkami.",
    en: "terms and conditions.",
    de: "xxx"
  },
  yourAccount: {
    cz: "Váš účet",
    en: "Account",
    de: "xxx"
  },
  account: {
    cz: "Účet",
    en: "Account",
    de: "xxx"
  },
  lengthPalicek: {
    cz: "délka paliček",
    en: "length of drumsticks",
    de: "xxx"
  },
  weightPalicek: {
    cz: "průměr paliček",
    en: "diameter of drumsticks",
    de: "xxx"
  },
  logOut: {
    cz: "Odhlásit se",
    en: "Log out",
    de: "xxx"
  },
  save: {
    cz: "ULOŽIT",
    en: "Save",
    de: "xxx"
  },
  orderHistory: {
    cz: "Historie objednávek",
    en: "Order history",
    de: "xxx"
  },
  noOrder: {
    cz: "Zatím nemáte žádnou objednávku",
    en: "You don't have any orders yet",
    de: "xxx"
  },
  orderNumber: {
    cz: "Objednávka č.",
    en: "Order no.",
    de: "xxx"
  },
  deliveryFreeCanvas: {
    cz: "Doprava ZDARMA po ČR a SK",
    en: "Free delivery to DE and AU",
    de: "xxx"
  },
  saleCanvas: {
    cz: "Sleva 5 % na všechny produkty",
    en: "5% discount on all products",
    de: "xxx"
  },
  deliveryFreeCanvasValue: {
    cz: "nad 1500 Kč",
    en: "over 100 €",
    de: "xxx"
  },
  saleCanvasValue: {
    cz: "nad 2000 Kč",
    en: "over 150 €",
    de: "xxx"
  },
  companyName: {
    cz: "Obchodní jméno",
    en: "Company name",
    de: "xxx"
  },
  ico: {
    cz: "IČO",
    en: "Company Registration No.",
    de: "xxx"
  },
  dic: {
    cz: "DIČ",
    en: "VAT no.",
    de: "xxx"
  },
  loginErrorExist: {
    cz: "Uživatel s tímto e-mailem již existuje.",
    en: "User with this email already exists.",
    de: "xxx"
  },
  loginErrorWrong: {
    cz: "Neplatý e-mail nebo heslo.",
    en: "Invalid email or password.",
    de: "xxx"
  },
  emptyFields: {
    cz: "Vyplňte prosím všechna pole.",
    en: "Please fill in all fields.",
    de: "xxx"
  },
  sale: {
    cz: "Sleva 5%",
    en: "Discount 5%",
    de: "xxx"
  },

	  // odsud


  PayStatusCash: {
    cz: "Platba na dobírku",
    en: "Payment on delivery",
    de: "xxx"
  },
  PayStatusError: {
    cz: "Platba se nezdařila",
    en: "Payment failed",
    de: "xxx"
  },
  PayStatusOk: {
    cz: "Platba zaplacena",
    en: "Transaction completed",
    de: "xxx"
  },
  PayStatusWait: {
    cz: "Čeká na zaplacení",
    en: "Awaiting payment",
    de: "xxx"
  },
  selectDeliveryError: {
    cz: "Vyberte způsob dopravy",
    en: "Select a delivery method",
    de: "xxx"
  },
  selectPayMehodError: {
    cz: "Vyberte způsob platby",
    en: "Select the payment method",
    de: "xxx"
  },
  sendResetPasswordButton: {
    cz: "Odeslat požadavek",
    en: "Submit a request",
    de: "xxx"
  },
  thankOrder: {
    cz: "Děkujeme za Vaši objednávku",
    en: "Thank you for your order",
    de: "xxx"
  },
  thankInfo: {
    cz: "Na Vámi uvedený e-mail bylo zasláno potvrzení o provedené objednávce.",
    en: "A confirmation of the order has been sent to your e-mail.",
    de: "xxx"
  },
  errorSendOrder: {
    cz: "Chyba, zkontrolujte si prosím vaše údaje.",
    en: "Error, please check your details.",
    de: "xxx"
  },
  linkBuisness: {
    cz: "/clanek/kategorie/vseobecne-obchodni-podminky",
    en: "/en/clanek/kategorie/agb",
    de: "xxx"
  },
  showResults: {
    cz: "Zobrazit výsledky",
    en: "Show results",
    de: "xxx"
  },
  cancelFilters: {
    cz: "Zrušit všechny filtry",
    en: "Cancel all filters",
    de: "xxx"
  }
}
