const Loader = () => {
  return (
    <div className="loader">
      <span className="uk-margin-small-right" uk-spinner="ratio: 3"></span>
    </div>
  )
}

export default Loader
