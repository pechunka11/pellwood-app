import Total from './Total'

const TotalWrap = ({sum, sale}) => {

  return <Total sum={sum} sale={sale} />
}

export default TotalWrap
