import AnimateHeight from 'react-animate-height';
import translate from '../../data/staticTranslate'
import { useRouter } from 'next/router'
import Delivery from './components/delivery.js'
import Corporate from './components/corporate.js'
import Password from './components/password.js'
import Note from './components/note.js'
import ShipPay from './components/shipPay.js'

import localize from '../../data/localize'

const Checkout = ({
  state,
  error,
  setError,
  user,
  sumBefore,
  anotherAdress,
  companyData,
  password,
  note,
  deliveryMethod,
  errorAnother,
  setErrorAnother,
  paymentMethod,
  handleChange,
  onBlur
}) => {

  const router = useRouter()
  const {lang} = localize(router.locale)

  return(
    <div className="tm-checkout">
      <form id="checkout-form">
        <fieldset className="uk-fieldset">

          <legend className="uk-legend">{translate.deliveryDetails[lang]}</legend>

          <Delivery
            state={state[0]}
            setState={state[1]}
            error={error}
            setError={setError}
            onBlur={onBlur} />

          <div>
            <div className="uk-margin checkbox_item">
              <input type="checkbox" id="checkbox_another_address" onChange={() => handleChange('anotherAddressCheck', !state[0].anotherAddressCheck)} checked={state[0].anotherAddressCheck} />
              <label htmlFor="checkbox_another_address"></label>
              <label htmlFor="checkbox_another_address">{translate.checkdifferentadress[lang]}</label>
            </div>

            <AnimateHeight duration={ 500 } height={ state[0].anotherAddressCheck ? 'auto' : 0 } >
              <Delivery
                state={anotherAdress[0]}
                setState={anotherAdress[1]}
                error={errorAnother}
                setError={setErrorAnother}
                onBlur={onBlur}/>
            </AnimateHeight>
          </div>


          <div className="">
            <div className="uk-margin checkbox_item">
              <input type="checkbox" id="checkbox_firm_data" onChange={() => handleChange('companyDataCheck', !state[0].companyDataCheck)} checked={state[0].companyDataCheck} />
              <label htmlFor="checkbox_firm_data"></label>
              <label htmlFor="checkbox_firm_data">{translate.checkcompanydata[lang]}</label>
            </div>

            <AnimateHeight duration={ 500 } height={ state[0].companyDataCheck ? 'auto' : 0 } >
              <Corporate state={companyData[0]} setState={companyData[1]} />
            </AnimateHeight>
          </div>


          {/*{user.email === undefined ? <div className="">
              <div className="uk-margin checkbox_item">
                <input type="checkbox" id="checkbox_registration" onChange={() => handleChange('registrationCheck', !state[0].registrationCheck)} checked={state[0].registrationCheck} />
                <label htmlFor="checkbox_registration"></label>
                <label htmlFor="checkbox_registration">{translate.chcekcreatecaccout[lang]}</label>
              </div>
          
              <AnimateHeight duration={ 500 } height={ state[0].registrationCheck ? 'auto' : 0 } >
                <Password state={password[0]} setState={password[1]}/>
              </AnimateHeight>
            </div> : ''}*/}

          <div className="">
            <div className="uk-margin checkbox_item">
              <input type="checkbox" id="checkbox_note" onChange={() => handleChange('noteCheck', !state[0].noteCheck)} checked={state[0].noteCheck} />
              <label htmlFor="checkbox_note"></label>
              <label htmlFor="checkbox_note">{translate.chceknote[lang]}</label>
            </div>

            <AnimateHeight duration={ 500 } height={ state[0].noteCheck ? 'auto' : 0 } >
              <Note state={note[0]} setState={note[1]} />
            </AnimateHeight>
          </div>

          <ShipPay
            error={error}
            setError={setError}
            delivery={deliveryMethod[0]}
            setDelivery={deliveryMethod[1]}
            payment={paymentMethod[0]}
            sumBefore={sumBefore}
            setPayment={paymentMethod[1]}/>

        </fieldset>
      </form>
    </div>
  )
}


export default Checkout
