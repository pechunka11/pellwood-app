const Head = ({head, lang}) => {
  return (
    <div className="tm-basket-head">
      <h1>{head}</h1>
    </div>
  )
}

export default Head
